<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert(
            [
                'title' => 'Produkt 1'
            ]
        );
        DB::table('products')->insert(
            [
                'title' => 'Produkt 2'
            ]
        );
        DB::table('products')->insert(
            [
                'title' => 'Produkt 3'
            ]
        );
    }
}
