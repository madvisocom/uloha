<?php

Route::get('/', 'ProductsController@index')->name('index');
Route::get('produkt/{id}', 'ProductsController@show')->name('show');
