# Uloha

## Installation
```python
git pull
composer install
cp .env.example .env
php artisan key:generate
php artisan migrate
php artisan db:seed
```

## ToDo
### Uloha 1
V priecinku storage/app/public/product_images budeme ukladat obrazky k produktom s nazvom ID_PRODUKTU-{1-x}.jpg. Kazdy produkt bude mat iny pocet obrazkov.
Treba si vytvorit testovacie obrazky a na detaile produktu (/produkt/1) zobrazit obrazky pre dany produkt. Cize najst obrazok 1-1.jpg a pokial existuje zobrazit. Pokracujeme kym nenajdeme a nezobrazime posledny obrazok (Pokial neexistuje obrazok napr. 1-5.jpg koncime, 1-6.jpg uz nehladame)
```
https://laravel.com/docs/5.6/filesystem#the-public-disk
```

### Uloha 2
Vytvorit cron job, ktory kazdy den o 4:00 prejde vsetky produkty, najde k nim obrazky a ulozi do DB. (Modely aj tabulky su pripravene)
```
https://laravel.com/docs/5.6/artisan#writing-commands
https://laravel.com/docs/5.6/scheduling#scheduling-artisan-commands
```

### Odovzdanie
Po dokonceni (kludne ciastkovom) vytvorit vlastnu branchu a pull request

## Kontakt
V pripade dalsich otazok kludne piste na kamil.piskorik@madviso.com