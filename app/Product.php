<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = ['title'];


    public function products()
    {
        return $this->belongsToMany('App\Image')->withTimestamps();
    }
}
